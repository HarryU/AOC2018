const PUZZLE: &str = include_str!("day_10.txt");

fn main() {
    for line in PUZZLE.lines() {
        parse_line(line);
    }
}

fn parse_line(input_line: &str) -> (i32, i32, i32, i32) {
    println!("{:?}", input_line);
    (1, 1, 1, 1)
}

#[test]
fn get_position_and_velocity_from_line() {
    assert_eq!(
        (1, 1, 1, 1),
        parse_line("position=< 1,  1> velocity=< 1,  1>")
    );
}

#[test]
fn test_part_one() {
    for line in PUZZLE.lines() {
        parse_line(line);
    }
}
