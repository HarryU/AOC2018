import numpy as np
import cv2


xs = []
ys = []

for line in open("day_6.txt"):
    x, y = map(int, line.strip().split(", "))
    xs.append(x)
    ys.append(y)

grid = np.zeros((max(ys) + 1, max(xs) + 1))

for value, (x, y) in enumerate(zip(xs, ys)):
    grid[y, x] = value + 1

nearest_grid = np.zeros_like(grid)
coords = np.array([ys, xs]).T

for x in range(max(xs) + 1):
    for y in range(max(ys) + 1):
        y_difference = np.abs(np.subtract(coords[:, 1], y))
        x_difference = np.abs(np.subtract(coords[:, 0], x))
        distances = np.add(y_difference, x_difference)
        if np.size(np.argwhere(distances == np.min(distances))) == 1:
            nearest_grid[y, x] = -1
        else:
            nearest_coord = coords[np.argmin(distances)]
            nearest_grid[y, x] = grid[nearest_coord[0], nearest_coord[1]]

np.set_printoptions(linewidth=np.nan, threshold=np.nan)
print(nearest_grid)
