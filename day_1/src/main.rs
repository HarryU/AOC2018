#![feature(cell_update)]

const PUZZLE: &str = include_str!("day1.txt");
use std::collections::HashSet;

fn main() {
   println!("Part one: {}", part_one(PUZZLE));
   println!("Part Two: {}", part_two(PUZZLE));
}

fn part_one(puzzle_input: &str) -> i32 {
    let mut value = 0;
    for line in puzzle_input.lines() {
        value = add_string(value, line);
    }
    value
}

fn part_two(puzzle_input: &str) -> i32 {
    let mut set = HashSet::new();
    let mut sum = 0;

    for freq in puzzle_input.lines().cycle() {
        sum = add_string(sum, freq);
        if !set.insert(sum) {
            return sum
        }
    }
    sum
}

fn add_string(current_value: i32, operation: &str) -> i32 {
    current_value + operation.parse::<i32>().unwrap()
}

#[test]
fn returns_1_with_0_and_plus_1() {
    assert_eq!(1, add_string(0, "+1"));
}

#[test]
fn returns_2_with_0_and_plus_2() {
    assert_eq!(2, add_string(0, "+2"));
}

#[test]
fn returns_0_with_minus_1_and_plus_1() {
    assert_eq!(0, add_string(-1, "+1"));
}

#[test]
fn test_part_one() {
    assert_eq!(470, part_one(PUZZLE));
}

#[test]
fn test_part_two() {
    assert_eq!(790, part_two(PUZZLE));
}
