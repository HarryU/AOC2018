import re
from datetime import datetime, timedelta
from collections import defaultdict, Counter


regex = re.compile(r'[(\d+)\-(\d+)\-(\d+) (\d+):(\d+) (*)]')
guards = []
for line in open("day_4.txt"):
    day_and_time, activity = line.strip().split('] ')
    matches = map(int, re.findall(r'\d+', day_and_time))
    date = datetime(*matches)
    guards.append((date, activity))


guards = sorted(guards, key=lambda x: x[0])
current_guard = 0
awake = True
guard_sleeps = defaultdict(lambda: ([], []))


for time, activity in guards:
    if '#' in activity:
        current_guard = int(re.findall(r'\d+', activity)[0])
    elif 'sleep' in activity:
        sleep_time = time
    elif 'wake' in activity:
        time_asleep = time - sleep_time
        guard_sleeps[current_guard][0].append(time_asleep.seconds)
        guard_sleeps[current_guard][1].extend(list(range(sleep_time.minute, time.minute)))


def find_most_common_minute(guard, sleeps):
    return Counter(sleeps[guard][1]).most_common(1)[0]


sleepiest_guard = sorted([(sum(ts), guard) for guard, (ts, _) in guard_sleeps.items()], key=lambda x: x[0])[-1][1]
most_common_minute = find_most_common_minute(sleepiest_guard, guard_sleeps)[0]
print('Part 1: ', most_common_minute * sleepiest_guard)

highest_freq = 0
for guard in guard_sleeps.keys():
    minute = find_most_common_minute(guard, guard_sleeps)
    if minute[1] > highest_freq:
        most_common_minute = minute[0]
        most_common_sleeper = guard
        highest_freq = minute[1]

print(most_common_sleeper, most_common_minute)
print('Part 2: ', most_common_minute * most_common_sleeper)
