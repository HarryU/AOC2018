#[macro_use]
extern crate lazy_static;
extern crate regex;

use regex::Regex;

const PUZZLE: &str = include_str!("day_5.txt");

fn main() {
    println!("{:?}", remove_pairs(PUZZLE.to_string()));
}

fn remove_pairs(mut string: String) -> String {
    string.pop();
    let mut v = Vec::new();
    for c in string.chars() {
        match v.last() {
            None => v.push(c),
            Some(&d) => if d.to_ascii_lowercase() == c.to_ascii_lowercase() && d != c {
                v.pop();
            } else {
                v.push(c);
            }
        }
    }
    v.into_iter().collect::<String>()
}

#[test]
fn test_string_with_no_pairs() {
    assert_eq!("ab", remove_pairs("ab\n".to_string()));
}

#[test]
fn test_string_with_only_pair() {
    assert_eq!("", remove_pairs("aA\n".to_string()));
}

#[test]
fn test_doesnt_remove_matching_case() {
    assert_eq!("aa", remove_pairs("aa\n".to_string()));
}

#[test]
fn test_removes_when_capital_comes_first() {
    assert_eq!("", remove_pairs("Aa\n".to_string()));
}

#[test]
fn test_removes_second_match_thats_not_present_initially() {
    assert_eq!("", remove_pairs("bAaB\n".to_string()));
}

#[test]
fn part_one_example() {
    assert_eq!(10, remove_pairs("dabAcCaCBAcCcaDA\n".to_string()).len());
}

#[test]
fn part_one() {
    assert_eq!(9808, remove_pairs(PUZZLE.to_string()).len());
}

#[test]
fn part_two() {
    let mut min = PUZZLE.len();
    for i in 0..26 {
        let mut string = PUZZLE.to_string();
        let char_to_remove = (('a' as u8) + i) as char;
        let other_char = char_to_remove.to_ascii_uppercase();
        let reduced_string = string.replace(char_to_remove, "");
        let reduced_string = reduced_string.replace(other_char, "");
        let condensed = remove_pairs(reduced_string).len();
        if condensed < min {
            min = condensed;
        }
    }
    assert_eq!(6484, min);
}
