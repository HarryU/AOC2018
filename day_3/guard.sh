#!/bin/sh
while true;
do
    pytest day3.py $@;
    inotifywait -e CLOSE_WRITE `git ls-files | grep *.py`;
    clear;
done
