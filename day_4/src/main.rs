#[macro_use]
extern crate lazy_static;

extern crate regex;

use regex::Regex;
use std::str::FromStr;

struct Time {
    year : usize,
    month: usize,
    day  : usize,
    hour : usize,
    minute: usize,
}

impl FromStr for Time {
    type Err = ();

    fn from_str(string: &str) -> Result<Self, Self::Err> {
         lazy_static! {
             static ref REGEX: Regex = Regex::new(r"[(\d+)-(\d+)-(\d+) (\d+):(\d+)]*").unwrap();
         }
         let capture_groups = REGEX.captures(string).unwrap();
         Ok(Time {
             year: capture_groups[1].parse::<usize>().unwrap(),
             month: capture_groups[2].parse::<usize>().unwrap(),
             day: capture_groups[3].parse::<usize>().unwrap(),
             hour: capture_groups[4].parse::<usize>().unwrap(),
             minute: capture_groups[5].parse::<usize>().unwrap(),
         })
    }
}

fn main() {
    println!("Hello, world!");
}
