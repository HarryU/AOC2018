from numpy.testing import assert_array_equal, assert_array_almost_equal
import numpy as np
import re

def increment_array(top_x, top_y, width, height, array):
    array[top_x:top_x + width, top_y:top_y + height] += 1
    return array

def check_claim(puzzle_input):
    array = np.zeros((1000, 1000))
    strings = []
    for line in puzzle_input:
        id_no, top_x, top_y, width, height = parse_string(line)
        array = increment_array(top_x, top_y, width, height, array)
        strings.append((id_no, top_x, top_y, width, height))
    for line in strings:
        id_no, top_x, top_y, width, height = line
        if check_if_intact(array[top_x:top_x + width, top_y:top_y + height]):
            return id_no

def parse_string(string):
    return tuple(map(int, re.findall(r'\d+', string)))


def count_overlap(input_array):
    return np.size(np.where(input_array.flatten() > 1)[0])


def check_if_intact(region):
    return np.all(region == 1)


def test_set_all_array_values_to_id():
    result = np.ones((3, 3))
    assert_array_equal(result, increment_array(0, 0, 3, 3, np.zeros((3, 3))))


def test_set_larger_array_to_id_of_1():
    result = np.ones((5, 5))
    assert_array_equal(result, increment_array(0, 0, 5, 5, np.zeros((5, 5))))


def test_increment_array_to_id_of_1_that_doesnt_start_at_corner():
    result = np.ones((6, 6))
    result[0, :] = 0
    result[:, 0] = 0
    assert_array_almost_equal(result, increment_array(1, 1, 5, 5, np.zeros((6, 6))))


def test_increment_array_that_has_an_initial_array():
    result = np.ones((5, 5))
    result[-1, :] = 0
    result[:, -1] = 0
    assert_array_almost_equal(result, increment_array(0, 0, 4, 4, np.zeros((5, 5))))


def test_increment_array_that_has_a_non_zero_initial_array():
    result = np.ones((5, 5))
    result[1:3, 1:3] = 2
    assert_array_almost_equal(result, increment_array(1, 1, 2, 2, np.ones((5, 5))))


def test_string_parsing():
    assert (1, 0, 0, 1, 1) == parse_string("#1 @ 0,0: 1x1")


def test_another_string_parsing():
    assert (20, 0, 0, 2, 2) == parse_string("#20 @ 0,0: 2x2")


def test_count_regions_with_overlap():
    input_array = np.zeros((8, 8))
    input_array[3:8, 1:-1] = 1
    input_array[1:3, 3:-1] = 1
    input_array[3:5, 3:5] = 2
    assert 4 == count_overlap(input_array)


def test_check_for_intact_claim():
    region = np.ones((3, 3))
    assert check_if_intact(region)


def test_check_not_intact_claim():
    region = np.full((3, 3), 2)
    assert not check_if_intact(region)


def test_part_one():
    input_array = np.zeros((1000, 1000))
    for line in open('day_3.txt'):
        id_no, top_x, top_y, width, height = parse_string(line)
        input_array = increment_array(top_x, top_y, width, height, input_array)
    assert count_overlap(input_array) == 104241


def test_part_two():
    assert check_claim(open('day_3.txt')) == 3
