use std::collections::{BTreeMap, BTreeSet};

const PUZZLE: &str = include_str!("day_7.txt");

fn main() {
    let mut input = BTreeMap::new();
    input.insert("A".chars().next().unwrap(), vec!["B".chars().next().unwrap()]);
    input.insert("B".chars().next().unwrap(), Vec::new());
    execution_order(input, 1, 0);
}

fn parse_letters(input_string: &str) -> [char; 2] {
    let characters: Vec<_> = input_string.split("").collect();
    [characters[6].chars().next().unwrap(), characters[37].chars().next().unwrap()]
}

fn schedule_letters(
    mut current_schedule: BTreeMap<char, Vec<char>>,
    before: char,
    new: char,
    ) -> BTreeMap<char, Vec<char>> {
    current_schedule.entry(new).or_default().push(before);
    current_schedule.entry(before).or_default();
    current_schedule
}

struct Worker {
    time_left: u32,
    current_step: Option<char>,
    time: u32,
}

impl Worker {
    fn tick(&mut self) {
        self.time_left = self.time_left.saturating_sub(1);
    }

    fn set_work(&mut self, step: char) {
        self.current_step = Some(step);
        if self.time == 0 {
            self.time_left = 0;
        } else if self.time == 1 {
            self.time_left = (step as u32) - ('A' as u32) + 1;
        } else {
            self.time_left = self.time + (step as u32) - ('A' as u32) + 1;
        }
    }
}

fn execution_order(schedule: BTreeMap<char, Vec<char>>, worker_count: u32, time: u32) -> String {
    let mut workers: Vec<Worker> = Vec::new();
    for i in 0..worker_count {
        workers.push(Worker { time_left: 0u32, current_step: None, time: time });
    }
    let mut order: Vec<char> = Vec::new();
    let mut worked: BTreeSet<char> = BTreeSet::new();
    let mut steps = schedule.keys().collect::<Vec<&char>>();
    steps.sort();
    let number_of_steps = schedule.iter().len();
    let mut i: i32 = 0;
    let mut time = 0;
    loop {
        println!("taken {:?}s so far", time);
        if order.len() == steps.len() { break; }
        let mut something_to_tick = false;
        let mut work_available = false;
        for worker in workers.iter_mut() {
            if worker.time_left == 0 {
                if let Some(finished_step) = worker.current_step.take() {
                    order.push(finished_step);
                    worked.remove(&finished_step);
                }
                }
            let mut ready_work = steps.iter().filter(|step| !order.contains(step) & !worked.contains(step) & (schedule.get(step).unwrap().iter().filter(|a| !order.contains(a)).collect::<Vec<&char>>().len() == 0)).collect::<Vec<&&char>>();
            work_available = ready_work.len() > 0;
            ready_work.reverse();
            println!("currently doing {:?}", worked);
            println!("already finished {:?}", order);
            println!("ready to start {:?}", ready_work);
            let step = ready_work.pop();
            if worker.time_left == 0 {
                let step = match step {
                    Some(step) => step,
                    None => continue,
                };
                worker.set_work(**step);
                worked.insert(**step);
                println!("currently doing {:?}", worked);
                println!("already finished {:?}", order);
            }
        }
        let available_workers = (worker_count as usize - worked.len()) > 0;
        println!("any workers? {:?}", available_workers);
        if !(available_workers & work_available) {
            for worker in workers.iter_mut() {
                worker.tick();
            }
            time += 1;
        }
    }
    println!("time taken: {:?}", time);
    order.iter().collect::<String>()
}

#[test]
fn test_two_letters_parsed() {
    assert_eq!(
        ["L".chars().next().unwrap(), "T".chars().next().unwrap()],
        parse_letters("Step L must be finished before step T can begin.")
        );
}

#[test]
fn test_another_two_letters_parsed() {
    assert_eq!(
        ["A".chars().next().unwrap(), "P".chars().next().unwrap()],
        parse_letters("Step A must be finished before step P can begin.")
        );
}

#[test]
fn test_schedules_first_two_letters() {
    let mut result = BTreeMap::new();
    result.insert("A".chars().next().unwrap(), Vec::new());
    result.insert("B".chars().next().unwrap(), vec!["A".chars().next().unwrap()]);
    assert_eq!(result, schedule_letters(BTreeMap::new(), "A".chars().next().unwrap() as char, "B".chars().next().unwrap() as char));
}

#[test]
fn test_schedules_third_letter() {
    let mut result = BTreeMap::new();
    result.insert("A".chars().next().unwrap(), Vec::new());
    result.insert("B".chars().next().unwrap(), vec!["A".chars().next().unwrap()]);
    result.insert("C".chars().next().unwrap(), vec!["A".chars().next().unwrap()]);
    let mut input = BTreeMap::new();
    input.insert("A".chars().next().unwrap(), Vec::new());
    input.insert("B".chars().next().unwrap(), vec!["A".chars().next().unwrap()]);
    assert_eq!(result, schedule_letters(input, "A".chars().next().unwrap(), "C".chars().next().unwrap()));
}

#[test]
fn test_adds_new_letter_between_existing_letters() {
    let mut result = BTreeMap::new();
    result.insert("A".chars().next().unwrap(), Vec::new());
    result.insert("B".chars().next().unwrap(), vec!["A".chars().next().unwrap()]);
    result.insert("C".chars().next().unwrap(), vec!["A".chars().next().unwrap()]);
    let mut input = BTreeMap::new();
    input.insert("A".chars().next().unwrap(), Vec::new());
    input.insert("C".chars().next().unwrap(), vec!["A".chars().next().unwrap()]);
    assert_eq!(result, schedule_letters(input, "A".chars().next().unwrap(), "B".chars().next().unwrap()));
}

#[test]
fn test_get_execution_order_from_schedule() {
    let mut input = BTreeMap::new();
    input.insert("A".chars().next().unwrap(), vec!["B".chars().next().unwrap()]);
    input.insert("B".chars().next().unwrap(), Vec::new());
    assert_eq!("BA", execution_order(input, 1, 0));
}

#[test]
fn test_get_different_execution_order_from_schedule() {
    let mut input = BTreeMap::new();
    input.insert("A".chars().next().unwrap(), vec!["C".chars().next().unwrap()]);
    input.insert("C".chars().next().unwrap(), Vec::new());
    assert_eq!("CA", execution_order(input, 1, 0));
}

#[test]
fn part_one() {
    let mut schedule = BTreeMap::new();
    for line in PUZZLE.lines() {
        let letters = parse_letters(line);
        schedule = schedule_letters(schedule, letters[0], letters[1]);
    }
    let order = execution_order(schedule, 1, 0);
    assert_eq!("ABDCJLFMNVQWHIRKTEUXOZSYPG", order);
}

#[test]
fn execution_order_with_a_second_worker() {
    let mut schedule = BTreeMap::new();
    schedule.insert("A".chars().next().unwrap(), vec!["C".chars().next().unwrap()]);
    schedule.insert("B".chars().next().unwrap(), vec!["A".chars().next().unwrap()]);
    schedule.insert("C".chars().next().unwrap(), Vec::new());
    schedule.insert("D".chars().next().unwrap(), vec!["A".chars().next().unwrap()]);
    schedule.insert("E".chars().next().unwrap(), vec!["B".chars().next().unwrap(), "D".chars().next().unwrap(), "F".chars().next().unwrap()]);
    schedule.insert("F".chars().next().unwrap(), vec!["C".chars().next().unwrap()]);
    assert_eq!("CABFDE", execution_order(schedule, 2, 1));
}

#[test]
fn part_two() {
    let mut schedule = BTreeMap::new();
    for line in PUZZLE.lines() {
        let letters = parse_letters(line);
        schedule = schedule_letters(schedule, letters[0], letters[1]);
    }
    let order = execution_order(schedule, 5, 60);
    assert_eq!("ABDCJLFMNVQWHIRKTEUXOZSYPG", order);
}
