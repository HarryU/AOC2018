const PUZZLE: &str = include_str!("day_2.txt");

fn main() {
    println!("Hello, world!");
}

fn string_has_pairs(input_string: &str) -> bool {
    string_has_count_matching(input_string, 2)
}

fn string_has_triples(input_string: &str) -> bool {
    string_has_count_matching(input_string, 3)
}

fn string_has_count_matching(input_string: &str, count: usize) -> bool {
    for c in input_string.chars() {
        let n = input_string.matches(c).count();
        if n == count {
            return true;
        }
    }
    false
}

fn checksum(input_string: &str) -> i32 {
    let mut twos = 0;
    let mut threes = 0;
    for string in input_string.lines() {
        if string_has_pairs(&string) {
            twos += 1;
        }
        if string_has_triples(&string) {
            threes += 1;
        }
    }
    return twos * threes;
}

fn nearly_matching_letters(one: &str, two: &str) -> String {
    let mut matches = Vec::new();
    for (letter_one, letter_two) in one.chars().zip(two.chars()) {
        if letter_one == letter_two {
            matches.push(letter_one);
        }
    }
    matches.into_iter().collect()
}

fn all_nearly_matching_strings(strings: &str) -> Vec<String> {
    let mut lines = strings.lines();
    let mut all_matches = Vec::new();
    while lines.clone().count() > 1 {
        let string = lines.next().unwrap();
        let rest = lines.clone();
        for other in rest {
            let matches = nearly_matching_letters(string, other);
            if matches.len() == (string.len() - 1) {
                all_matches.push(matches);
            }
        }
    }
    all_matches
}

#[test]
fn check_a_string_for_pairs_with_no_pairs() {
    assert_eq!(false, string_has_pairs("abc"));
}

#[test]
fn check_a_string_for_pairs_with_pairs() {
    assert_eq!(true, string_has_pairs("aa"));
}

#[test]
fn check_a_string_for_triple_with_no_triple() {
    assert_eq!(false, string_has_triples("abc"));
}

#[test]
fn check_a_string_for_triple_with_no_triple_but_a_double() {
    assert_eq!(false, string_has_triples("aac"));
}

#[test]
fn get_checksum_for_two_strings_with_no_pairs_or_triples() {
    assert_eq!(0, checksum("abc\ndef"));
}

#[test]
fn get_checksum_for_strings_with_a_pair_and_a_triple() {
    assert_eq!(1, checksum("aac\nddd"));
}

#[test]
fn get_checksum_for_strings_with_two_pairs_and_a_triple() {
    assert_eq!(2, checksum("aac\ndddee"));
}

#[test]
fn do_part_one() {
    assert_eq!(5166, checksum(PUZZLE));
}

#[test]
fn get_nearly_matching_letters_two_strings() {
    assert_eq!("a", nearly_matching_letters("a", "a"));
}

#[test]
fn get_nearly_matching_letters_two_more_strings() {
    assert_eq!("b", nearly_matching_letters("b", "b"));
}

#[test]
fn get_nearly_matching_letters_two_longer_strings() {
    assert_eq!("b", nearly_matching_letters("abc", "iba"));
}

#[test]
fn get_all_nearly_matching_strings() {
    assert_eq!(["b"].to_vec(), all_nearly_matching_strings("ab\nib"));
}

#[test]
fn get_different_nearly_matching_strings() {
    assert_eq!(["a"].to_vec(), all_nearly_matching_strings("ab\nac"));
}

#[test]
fn get_multiple_nearly_matching_strings() {
    assert_eq!(
        ["ab"].to_vec(),
        all_nearly_matching_strings("abc\nefg\nabd")
    );
}

#[test]
fn do_part_two() {
    assert_eq!(
        ["cypueihajytordkgzxfqplbwn"].to_vec(),
        all_nearly_matching_strings(PUZZLE)
    );
}
